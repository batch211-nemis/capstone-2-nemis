const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//User Registration
module.exports.registerUser = (data) =>{

return User.find({email:data.user.email}).then(result=>{

    if(result.length>0){
        let message = "Email already exists!"
            return message;

        } else {

            let newUser = new User({
                firstName : data.user.firstName,
                lastName : data.user.lastName,
                email : data.user.email,
                mobileNo : data.user.mobileNo,
                password : bcrypt.hashSync(data.user.password,10)
            })

            return newUser.save().then((user,error)=>{
                if(error){
                    return false;
                }else{
                    let message = "USER REGISTRATION SUCCESS!";
                    return {message, newUser};
                }
            })
        }
    })
}


//User Authentication
module.exports.loginUser = (reqBody) =>{
    return User.findOne({email:reqBody.email}).then(result=>{
        if(result==null){
            let message = "Email doesn't exists!";
            return message;
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
            if(isPasswordCorrect){
                let message = "WELCOME TO YOUR PROFILE!";
                return {message, access: auth.createAccessToken(result)};
            } else {
                let message = "Wrong Password!";
                return message;
            };
        };
    });
};


//Set User as Admin (Admin-only)
module.exports.updateAdmin = (data)=>{

	if (data.isAdmin){

		let updateActiveField = {isAdmin : true};

		return User.findByIdAndUpdate(data.userId, updateActiveField).then((user, error) => {

			if (error) {

				return false;

			} else {

				return User.findById(data.userId).then(result=>{
    
                    return result;
                });

			};

		});

	} else {
		return Promise.resolve("You are not authorized to make user an admin.");
	}
};

//Retrieve User Details
module.exports.getProfile = (data) =>{
    return User.findById(data.userId).then(result=>{
        result.password = "";
        return result;
    });
};

