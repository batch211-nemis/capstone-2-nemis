const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const auth = require("../auth");


//Add new Product
module.exports.addProduct = (data) =>{

	if (data.isAdmin){
		
		return Product.find({name:data.product.name}).then(result=>{

		    if(result.length>0){
		        let message = "Product name already exists!"
		            return message;

	        } else {
			
		    	let newProduct = new Product({
		        	name : data.product.name,
		        	description : data.product.description,
		        	price : data.product.price,
		        	stocks: data.product.stocks
		    		})

			    return newProduct.save().then((product,error)=>{
			        if(error){
			            return false;
			        } else {
			            let message = "PRODUCT CREATED SUCCESSFULLY!";
                    return {message, newProduct};
			        }
			    })
			}
		})

	} else {

		return Promise.resolve("You are not authorized to add a product.");
	}

}


//Retrieve all products
module.exports.getAllProducts = ()=>{
    return Product.find({}).then(result=>{
        return result;
    });
};


//Retrieve all active products
module.exports.getAllActiveProducts = ()=>{
    return Product.find({isActive:true}).then(result=>{
        return result;
    });
};


//Retrieving a single product
module.exports.getProduct = (reqParams)=>{
    return Product.findById(reqParams.productId).then(result=>{
        return result;
    });
};


//Updating a product
module.exports.updateProduct = (data)=>{

	if (data.isAdmin){

	    let updatedProduct = {
	        name: data.product.name,
	        description: data.product.description,
	        price: data.product.price,
	    };

    	return Product.findByIdAndUpdate(data.productId,updatedProduct).then((product,error)=>{
	        if(error){
	            return false;
	        } else {
	            return Product.findById(data.productId).then(result=>{return result;});
	        };
	    }); 

    }  else {
    	return Promise.resolve("You are not authorized to update a product.");
    }
};


//Archive a product
module.exports.archiveProduct = (data)=>{

	if (data.isAdmin){

	    let archivedProduct = {isActive: false};

    	return Product.findByIdAndUpdate(data.productId,archivedProduct).then((product,error)=>{
	        if(error){
	            return false;
	        } else {
	            return Product.findById(data.productId).then(result=>{return result;});
	        };
	    }); 

    }  else {
    	return Promise.resolve("You are not authorized to archive a product.");
    }
};


//Activate a product
module.exports.activateProduct = (data)=>{

	if (data.isAdmin){

	    let archivedProduct = {isActive: true};

    	return Product.findByIdAndUpdate(data.productId,archivedProduct).then((product,error)=>{
	        if(error){
	            return false;
	        } else {
	            return Product.findById(data.productId).then(result=>{return result;});
	        };
	    }); 

    }  else {
    	return Promise.resolve("You are not authorized to activate a product.");
    }
};



// //Add product to cart
// module.exports.addToCartProduct = (data) =>{

// 	let addToCart = {
// 		cart: [{
// 			productId:data.productId,
// 			quantity: data.quantity
// 		}],
// 	};

// 	return User.findByIdAndUpdate(data.userId, {$push: addToCart}).then((product,error)=>{

// 		let message = "You added an item to your cart successfully!";

// 	        if(error){
// 	            return false;
// 	        } else {
// 	            return message;      
// 	        };
// 	    }); 
// }

	
//  /*   let addToCart = User.findById(data.id).then(user=>{
//         user.cart.push({productId:data.productId});

//         return user.save().then((user,error)=>{
//             if(error){
//                 return false;
//             }else{
//                 return User.findById(userData.id).then(result=>{return result;});
//             }
//         })
//     })
// */

// //Change product quantities in cart
// module.exports.changeQuantity = (data) =>{



// 	 let updatedQuantity = {
// 	        quantity: data.cart.quantity
// 	    };

//     	return Product.findByIdAndUpdate(data.productId,updatedProduct).then((product,error)=>{
// 	        if(error){
// 	            return false;
// 	        } else {
// 	            return Product.findById(data.productId).then(result=>{return result;});
// 	        };
// 	    }); 


// 	let isUserUpdated = User.findById(data.userId).then(user=>{
//         user.cart.updateOne({quantity:data.quantity});

//         return user.save().then((user,error)=>{

//            let message = "You changed the product quantity in your cart successfully!";

// 	        if(error){
// 	            return false;
// 	        } else {
// 	            return message;      
// 	        };
// 	    });
// 	 });

// 		/*let setQuantity = {
// 		cart: [{
// 			quantity: data.cart.quantity
// 		}],
// 	};

// 	return User.findByIdAndUpdate(data.userId,setQuantity).then((changedQuantity,error)=>{

// 		let message = "You changed the product quantity in your cart successfully!";

// 	        if(error){
// 	            return false;
// 	        } else {
// 	            return message;      
// 	        };
// 	    }); */
// }

// //Remove product from cart
// module.exports.removeProduct = ()=>{
//     let removeProduct = User.findById(data.userId).then(user=>{
//         user.cart.deleteOne({productId:data.productId});

//         return user.save().then((user,error)=>{

//            let message = "Product deleted!";

// 	        if(error){
// 	            return false;
// 	        } else {
// 	            return message;      
// 	        };
// 	    });
// 	 });
// };
