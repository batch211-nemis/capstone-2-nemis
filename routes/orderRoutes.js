const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");


//Route for creating an order
router.post("/order", auth.verify, (req,res)=>{

	const data = {

		userId: auth.decode(req.headers.authorization).id,
		deliveryMode: req.body.deliveryMode,
		deliveryAddress: req.body.deliveryAddress,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	
	console.log(data);

	orderController.checkOut(data).then(resultFromController=>res.send(resultFromController));
});


//Route for retrieving All Orders (admin-only)
router.get("/", auth.verify, (req,res)=>{

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	console.log(data);

	orderController.getAllOrders(data).then(resultFromController=>res.send(resultFromController));
});


//Route for retrieving Authenticated User’s Orders
router.get("/getUserOrders", auth.verify, (req,res)=>{

	const data = {
		userId: auth.decode(req.headers.authorization).id,
	}
	
	console.log(data);

	orderController.getUserOrders(data).then(resultFromController=>res.send(resultFromController));
});


//Route for adding an order
router.put("/addOrder", auth.verify, (req,res)=>{

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	
	console.log(data);

	orderController.addOrder(data).then(resultFromController=>res.send(resultFromController));
});


//Route for retrieving orders for delivery
router.get("/forDelivery", auth.verify, (req,res)=>{

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	console.log(data);

	orderController.getDelOrders(data).then(resultFromController=>res.send(resultFromController));
});


//Route for retrieving orders for meet-up
router.get("/forPick-up", auth.verify, (req,res)=>{

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	console.log(data);

	orderController.getPickOrders(data).then(resultFromController=>res.send(resultFromController));
});


// //Route for transfer orders
// router.get("/completedOrders/:orderId",auth.verify,(req,res)=>{

// 	const data = {
// 		orderId: req.params.productId,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	console.log(data);

// 	orderController.transferOrders(data).then(resultFromController=>res.send(resultFromController));
// });

 module.exports = router;