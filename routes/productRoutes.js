const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");



//Route for creating a product (admin-only)
router.post("/addProduct", auth.verify, (req,res)=>{

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.addProduct(data).then(resultFromController=>res.send(resultFromController));
});


//Route for Retrieving all products
router.get("/",(req,res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController));
});


//Route for Retrieving all active the products
router.get("/active",(req,res)=>{
	productController.getAllActiveProducts().then(resultFromController=>res.send(resultFromController));
});


//Route for Retrieving a single product
router.get("/:productId",(req,res)=>{
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController));
});


//Route for Updating a product
router.put("/updateProduct/:productId",auth.verify,(req,res)=>{

	const data = {
		productId: req.params.productId,
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	productController.updateProduct(data).then(resultFromController=>res.send(resultFromController));
});


//Route for Archiving a product (Admin-only)
router.put("/archiveProduct/:productId",auth.verify,(req,res)=>{

	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	productController.archiveProduct(data).then(resultFromController=>res.send(resultFromController));
});


//Route for Activating a product (admin-only)
router.put("/activateProduct/:productId",auth.verify,(req,res)=>{

	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	productController.activateProduct(data).then(resultFromController=>res.send(resultFromController));
});





/*//Route for add to product to cart
router.put("/addtocart",auth.verify,(req,res)=>{

 	const data = {
		userId: auth.decode(req.headers.authorization).id,
 		productId: req.body.productId,
 		quantity: req.body.quantity
 	}

 	console.log(data);

 productController.addToCartProduct(data).then(resultFromController=>res.send(resultFromController));
});


//Route for changing quantities in cart
router.put("/changeQuantity/:productId",auth.verify,(req,res)=>{

 	const data = {
 		productId: req.params.productId,
		userId: auth.decode(req.headers.authorization).id,
 		quantity: req.body.quantity
 	}

 	console.log(data);

 productController.changeQuantity(data).then(resultFromController=>res.send(resultFromController));
});


//Route for removing a product in cart
router.delete("/removeProduct/:productId",auth.verify,(req,res)=>{

 	const data = {
 		productId: req.params.productId,
		userId: auth.decode(req.headers.authorization).id
 	}

 	console.log(data);

 productController.changeQuantity(data).then(resultFromController=>res.send(resultFromController));
});*/


module.exports = router;