const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");



//Route for user registration
router.post("/register",(req,res)=>{

	const data = {
		user: req.body
	}

	userController.registerUser(data).then(resultFromController=>res.send(resultFromController));
})

//Route for user authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});

//Route for setting user as Admin
router.put("/updateAdmin/:userId",auth.verify,(req,res)=>{

	const data = {
		userId: req.params.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	userController.updateAdmin(data).then(resultFromController=>res.send(resultFromController));
});

//Route for retrieving user details
router.get("/userDetails", auth.verify, (req,res)=>{

	const userData = auth.decode(req.headers.authorization);

    userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
})
module.exports = router;