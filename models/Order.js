const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "UserId is required"]
	},
	deliveryMode: {
		type: String,
		required: [true, "Delivery Mode is required"]
	},
	deliveryAddress: {
			type: String,
			required: [true, "Delivery Address is required"]
	},
	totalAmount: {
		type: Number,
		required:[true,"Total Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	orderStatus: {
		type: String,
		default: "Preparing"
	},
	products: [
		{
			productId:{
				type: String,
				required: [true,"Product ID is required"]
			},
			quantity:{
				type: Number,
				required: [true, "Quantity is required"]
			},
			subTotal:{
				type: Number,
				default: 0
			}
		}
	]
})

module.exports = mongoose.model("Order",orderSchema);
//module.exports = mongoose.model("OrderCompleted",orderSchema);